'use strict'
var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'clave_secreta_pagina';

exports.ensureAuth = function(req, res, next) {
    if (!req.headers.authorization) {
        res.status(403).send({ message: 'la cabecera no tiene la cabecera de autenticación' });
    }
    try {
        console.log(req.headers);
        var token = req.headers.authorization.replace(/['"]+/g, '');

        var payload = jwt.decode(token, secret);
        console.log(payload.exp);
        console.log(moment().format());
        if (payload.exp <= moment().format()) {
            console.log('token expirado');
            return res.status(401).send({ message: 'El token ha expirado' });
        }
        req.user = payload;
        next();
    } catch (ex) {
        console.log(ex);
        return res.status(404).send({ message: 'Token  no valido' });

    }
};